libnet-sslglue-perl (1.058-2) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.1.5, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 06 Dec 2022 18:28:07 +0000

libnet-sslglue-perl (1.058-1) unstable; urgency=medium

  * Team upload.

  [ Angel Abad ]
  * Import upstream version 1.058
  * debian/copyright: Update years.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/upstream/metadata: fix Contact field.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Tue, 10 Apr 2018 19:11:41 +0200

libnet-sslglue-perl (1.057-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Import upstream version 1.057
  * Declare compliance with Debian policy 3.9.8
  * Fix some spelling errors in manpage

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 22 May 2016 15:00:26 +0200

libnet-sslglue-perl (1.055-1) unstable; urgency=medium

  * Team upload.

  * Add debian/upstream/metadata.
  * debian/watch: use MetaCPAN URL.
  * debian/control: update Vcs-* fields.
  * debian/control: use MetaCPAN URL for Homepage field.

  * Import upstream version 1.055
  * Update years of upstream copyright.
  * Don't install almost empty README anymore.
  * Make short description a noun phrase.
  * debian/control: remove unneeded version constraints from (build)
    dependencies.
  * Mark package as autopkgtest-able.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Mon, 30 Nov 2015 23:47:53 +0100

libnet-sslglue-perl (1.054-1) unstable; urgency=medium

  * Switch to team maintenance
  * New upstream release (Closes: #789847)
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Thu, 27 Aug 2015 21:30:15 +0100

libnet-sslglue-perl (1.052-1) unstable; urgency=medium

  * Conform with copyright-format 1.0
  * New upstream release
  * Update to debhelper compatibility level 8
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 09 Feb 2014 15:14:02 +0000

libnet-sslglue-perl (1.04-1) unstable; urgency=low

  * New upstream release (Closes: #726409)
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 27 Oct 2013 14:18:07 +0000

libnet-sslglue-perl (1.01-1) unstable; urgency=low

  * New upstream release
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Fri, 06 Apr 2012 18:06:20 +0100

libnet-sslglue-perl (0.8-1) unstable; urgency=low

  * Add Vcs-* fields
  * New upstream release
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Mon, 29 Aug 2011 17:32:04 +0100

libnet-sslglue-perl (0.5-1) unstable; urgency=low

  * New upstream release (closes: #593555)
    - fix incorrect reference to startssl method (closes: #593556)
    - works with HTTPS proxies (closes: #593117)
  * Update Standards-Version (no changes)
  * Switch to minimal dh7 rules file
  * Update to debhelper compatibility level 7
  * Update source format version to 3.0 (quilt)
  * Install examples

 -- Dominic Hargreaves <dom@earth.li>  Sat, 05 Feb 2011 16:40:18 +0000

libnet-sslglue-perl (0.2-2) unstable; urgency=low

  * Tighten dependency on libwww-perl to a suitably recent version
    (closes: #534184)
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Wed, 01 Jul 2009 19:09:15 +0100

libnet-sslglue-perl (0.2-1) unstable; urgency=low

  * Initial Release (closes: #529623)

 -- Dominic Hargreaves <dom@earth.li>  Wed, 20 May 2009 22:47:53 +0100
